var bombasArray = [];
var bombasNextDoor;
var v;
var h;

onLoad = () => {
    let maxWidth;
    let maxHeight;
    var startGameDiv = document.createElement('div');
    startGameDiv.id = 'startGameDiv';
    startGameDiv.textContent = 'Let\'s play: ';
    document.body.appendChild(startGameDiv);
    var fieldWidth = document.createElement('input');
    fieldWidth.type = 'text';
    fieldWidth.id = 'hCellsQnty';
    fieldWidth.style = 'width: 2vh';
    fieldWidth.value = '9';

    // fieldWidth.onkeydown = "this.style.width = ((this.value.length + 1) * 8) + 'px'";
//<input type="text" id="vCellsQnty" value="9" style="width: 2vh" onkeydown="this.style.width = ((this.value.length + 1) * 8) + 'px';"><!--quantity of vertical cells-->
    startGameDiv.appendChild(fieldWidth);
    var asterixText = document.createElement('span');
    asterixText.textContent = ' * ';
    startGameDiv.appendChild(asterixText);
    var fieldHeight = document.createElement('input');
    fieldHeight.type = 'text';
    fieldHeight.id = 'vCellsQnty';
    fieldHeight.value = '9';
    fieldHeight.style = 'width: 2vh';
    fieldWidth.onkeydown = "this.style.width = ((this.value.length + 1) * 8) + 'px'";
    startGameDiv.appendChild(fieldHeight);
    var cellsText = document.createElement('span');
    cellsText.textContent = ' cells?  ';
    startGameDiv.appendChild(cellsText);
    var startGameBtn = document.createElement('input');
    startGameBtn.type = 'button';
    startGameBtn.id="startGameBtn";
    startGameBtn.value="Yes, let's start!";
    startGameBtn.onclick = startGame;
    startGameDiv.appendChild(startGameBtn);
};

var startGame = () => {
    bombasArray = [];
    h = document.getElementById('hCellsQnty').value;
    v = document.getElementById('vCellsQnty').value;

// Let's clear it
document.body.removeChild(document.querySelector('#startGameDiv'));
//========================
// Draw the mineField' controls
    let stopGameDiv = document.createElement('div');
    stopGameDiv.id = 'stopGameDiv';
    let stopGameBtn = document.createElement('button');
    stopGameBtn.textContent = 'Stop this insanity!';
    stopGameBtn.id = 'stopGameBtn';
    stopGameDiv.appendChild(stopGameBtn);
    stopGameBtn.onclick = stopGame;
    let br = document.createElement("br");
    document.body.appendChild(stopGameDiv);
    document.body.appendChild(br);
//========================
// Draw the mineField
    var fieldsQnty = h*v;
    var bombas = Math.floor(0.2*fieldsQnty + Math.random() * (0.4*fieldsQnty + 1 - 0.2*fieldsQnty));
    console.log(`There are ${bombas} bombs here`);

    var mineField = document.createElement('table');
    mineField.id = 'mineField';

    for(let i=0 ; i<h ; i++){
        var mineFieldRaw = document.createElement('tr');
        mineFieldRaw.id = 'raw'+i;
        mineField.appendChild(mineFieldRaw);

        for (let j=0 ; j<v ; j++){
            var mineFieldCell = document.createElement('td');
            mineFieldCell.id = 'cell-'+i+'-'+j;
            var mineFieldIncognitoIMG = document.createElement('img');
            mineFieldIncognitoIMG.src = './IMG/incognita.png';
            mineFieldIncognitoIMG.width = 25;
            mineFieldIncognitoIMG.height = 22;
            mineFieldIncognitoIMG.id = 'imgIncognita-'+i+'-'+j;
            mineFieldIncognitoIMG.addEventListener('click', fieldClick);
            mineFieldCell.appendChild(mineFieldIncognitoIMG);
            mineFieldRaw.appendChild(mineFieldCell);
        }
    }
    document.body.appendChild(mineField);
// Now, let's seed bombs
    seedMines(bombas, h, v);
};//start game

stopGame = () => {
    document.body.removeChild(document.querySelector('#stopGameDiv'));
    document.body.removeChild(document.querySelector('#mineField'));
    document.body.removeChild(document.querySelector('br'));
    if(document.querySelector('#offerNewGame')) document.body.removeChild(document.querySelector('#offerNewGame'));
    onLoad();
};

seedMines = (bombas, h, v) => {

var bombasOrder = [];
for(let i = 0 ; i < h*v ; i++){
    bombas > 0 ? bombasOrder.push(1) : bombasOrder.push(0);
    bombas--;
};
// taken from here https://qna.habr.com/q/163095
for (let i = bombasOrder.length-1 ; i > 0 ; i--){
    let j = Math.floor(Math.random() * (i + 1));
    let temp = bombasOrder[i];
    bombasOrder[i] = bombasOrder[j];
    bombasOrder[j] = temp;
};

    for (let y = 0 ; y < v ; y ++){
        let row = [];
        for (let x = 0 ; x < h ; x++){
            row.push(bombasOrder[y*h+x]);
        }
        bombasArray.push(row);
    }
};

function fieldClick () {
    console.log(`v = ${v}, h = ${h}`);
    var bomba = document.createElement('span');
    bomba.style = 'position: relative; top: 0;left: 0; width: 100%; margin: 0; z-index: 2';
    let clickedCell = this.id.substr(13).split('-');
    if (bombasArray[Number(clickedCell[0])][Number(clickedCell[1])] === 1) {
        for (let y = 0; y < v; y++) {
            for (let x = 0; x < h; x++) {
                document.querySelector(`#imgIncognita-${y}-${x}`).removeEventListener('click', fieldClick);
                if (bombasArray[y][x]===1)
                    document.querySelector(`#imgIncognita-${y}-${x}`).src = './IMG/bomba.png'
            }
        }
        document.querySelector(`#imgIncognita-${Number(clickedCell[0])}-${Number(clickedCell[1])}`).src = './IMG/fail.png'

        var offerNewGame = document.createElement('div');
        offerNewGame.id = 'offerNewGame';
        offerNewGame.textContent = 'The Game is over. Try again?';
        document.body.appendChild(offerNewGame);
        var offerNewGameBtn = document.createElement('input');
        offerNewGameBtn.type = 'button';
        offerNewGameBtn.id='offerNewGameBtn';
        offerNewGameBtn.value="OK, let's try again";
        offerNewGameBtn.onclick = reRun;
        let br = document.createElement("br");
        offerNewGame.appendChild(br);
        offerNewGame.appendChild(offerNewGameBtn);
    }else if(bombasArray[Number(clickedCell[0])][Number(clickedCell[1])] === 0){
        console.log('v: ', Number(clickedCell[0]), 'of', v, ' h: ', Number(clickedCell[1]), 'of', h);
        console.log('this: '+bombasArray[Number(clickedCell[0])][Number(clickedCell[1])]);
        let n, no, o, so, s, sw, w, nw;
console.log('-------------------------');

/*n*/  Number(clickedCell[0]) == 0 ? n = 0 : n = bombasArray[Number(clickedCell[0])-1][Number(clickedCell[1])]; //n
/*no*/ Number(clickedCell[0]) == 0 || Number(clickedCell[1]) == (h-1) ? no = 0 : no = bombasArray[(Number(clickedCell[0])-1)][(Number(clickedCell[1])+1)]; //no
/*o*/  Number(clickedCell[1]) == (h-1) ? o = 0 : o = bombasArray[Number(clickedCell[0])][Number(clickedCell[1])+1]; //o
/*so*/ Number(clickedCell[0]) == (v-1) || Number(clickedCell[1]) == (h-1) ? so = 0 : so = bombasArray[Number(clickedCell[0])+1][Number(clickedCell[1])+1]; //so
/*s*/  Number(clickedCell[0]) == (v-1) ? s = 0 : s = bombasArray[Number(clickedCell[0])+1][Number(clickedCell[1])]; //s
/*sw*/ Number(clickedCell[0]) == (v-1) || Number(clickedCell[1]) == 0 ? sw = 0 : sw = bombasArray[Number(clickedCell[0])+1][Number(clickedCell[1])-1]; //sw
/*w*/  Number(clickedCell[1]) == 0 ? w = 0 : w = bombasArray[Number(clickedCell[0])][Number(clickedCell[1])-1]; //w
/*nw*/ Number(clickedCell[0]) == 0 || Number(clickedCell[1]) == 0 ? nw = 0 : nw = bombasArray[Number(clickedCell[0])-1][Number(clickedCell[1])-1]; //nw
        console.log('v:', v, 'h', h, 'n:', n, 'no:', no, 'o:', o, 'so:', so, 's:', s, 'sw:', sw, 'w:', w, 'nw:', nw);
console.log('--------------------------');

        bombasNextDoor = n + no + o + so + s + sw + w +nw;
        console.log('bombasNextDor: ', bombasNextDoor);
console.log('==========================');
        switch (bombasNextDoor) {
            case 0:
                document.querySelector(`#imgIncognita-${Number(clickedCell[0])}-${Number(clickedCell[1])}`).src = './IMG/ok.png';
                break;
            case 1:
                document.querySelector(`#imgIncognita-${Number(clickedCell[0])}-${Number(clickedCell[1])}`).src = './IMG/1.png';
                break;
            case 2:
                document.querySelector(`#imgIncognita-${Number(clickedCell[0])}-${Number(clickedCell[1])}`).src = './IMG/2.png';
                break;
            case 3:
                document.querySelector(`#imgIncognita-${Number(clickedCell[0])}-${Number(clickedCell[1])}`).src = './IMG/3.png';
                break;
            case 4:
                document.querySelector(`#imgIncognita-${Number(clickedCell[0])}-${Number(clickedCell[1])}`).src = './IMG/4.png';
                break;
            case 5:
                document.querySelector(`#imgIncognita-${Number(clickedCell[0])}-${Number(clickedCell[1])}`).src = './IMG/5.png';
                break;
            case 6:
                document.querySelector(`#imgIncognita-${Number(clickedCell[0])}-${Number(clickedCell[1])}`).src = './IMG/6.png';
                break;
            case 7:
                document.querySelector(`#imgIncognita-${Number(clickedCell[0])}-${Number(clickedCell[1])}`).src = './IMG/7.png';
                break;
            case 8:
                document.querySelector(`#imgIncognita-${Number(clickedCell[0])}-${Number(clickedCell[1])}`).src = './IMG/8.png';
                break;
        };//SwitchCase
    };//clicking no-bombas field
};//field click

reRun = () => {
    document.body.removeChild(document.querySelector('#offerNewGame'));
    document.body.removeChild(document.querySelector('#stopGameDiv'));
    document.body.removeChild(document.querySelector('#mineField'));
    document.body.removeChild(document.querySelector('br'));
    onLoad();
}